const http = require("http");

const port = 3000;

const server = http.createServer(function (req, res) {
    if (req.url == "/login") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Welcome to login page.");
    } else {
        res.writeHead(404, { "Content-Type": "text/plain" });
        res.end("Page cannot be found!");
    }
});
server.listen(port);

console.log(`Server running at localhost: ${port}`);

let http = require("http");

http.createServer(function (req, res) {
    res.writeHead(200, { "Content-type ": "text/plain" });
    res.end("Hello World");
}).listen(4000);

console.log("Server running at localhost:4000");

const http = require("http");

const port = 4000;

const server = http.createServer(function (req, res) {
    if (req.url == "/greeting") {
        res.writeHead(200, { "Content-type": "text/plain" });
        res.end("Hello World");
    } else if (req.url == "/homepage") {
        res.writeHead(200, { "Content-type": "text/plain" });
        res.end("Welcome to the homepage!");
    } else {
        res.writeHead(404, { "Content-type": "text/plain" });
        res.end("Page not available!");
    }
});
server.listen(port);

console.log(`Server is now accessible at localhost ${port}`);
